const express = require('express');
const app = express();

app.use(bodyParser.json({
    limit: '55mb'
}));

app.use(bodyParser.urlencoded({
    extended: true,
    limit: '55mb'
}));

const port = process.env.PORT || 3000;

// Catching all exception on PROD
// if (keys.env === "PROD") {
    process.on('uncaughtException', function(err) {
        console.error('================Caught exception: ' + err);
    });
// }

app.listen(port, () => console.log(`${keys.env} server started on port ${port}`))
